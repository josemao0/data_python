#!/usr/bin/env python3
# -*- coding: utf-8 -*-


"""

Test to commit different file, bitbucket

Created on Tue Oct 22 07:10:02 2019
PRUEBA TECNICA DATAKNOW - 1-CARGAR INFORMACION
@author: jmao
"""
import pandas as pd

split_by_clm = []

path = "/home/jmao/Mao_files/DataKnow_Prueba_Tecnica/Datos3/"
path_save = "/home/jmao/Mao_files/DataKnow_Prueba_Tecnica/Resultados/Anexos_Prueba_1/"

headers = ["AGENTE","Planta","Hora_1","Hora_2","Hora_3", "Hora_4","Hora_5",
         "Hora_6","Hora_7","Hora_8", "Hora_9","Hora_10","Hora_11","Hora_12","Hora_13",
         "Hora_14", "Hora_15","Hora_16","Hora_17","Hora_18","Hora_19","Hora_20",
         "Hora_21","Hora_22","Hora_23","Hora_24"]

with open(path +"OFEI1204.txt" ,'r') as file:   #Open and read.txt file
   data = file.read() 
   
split_by_row = data.split("\n") #row separation by \n variable

for index in split_by_row:
    split_by_clm.append(index.split(',')) #Column separation by comma variable

table = pd.DataFrame(split_by_clm) #List to DataFrame convertion

table.columns = headers #headers definition 

table_D = table[table["Planta"] == " D"] #Data filtering by "D" condition
table_D = table_D.reset_index(drop=True) # Reset data indices
table_D.to_excel(path_save+"output_OFEI1204.xlsx") #Data saved in xlsx format
