#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 22 15:21:10 2019
PRUEBA TECNICA DATAKNOW - 2-MANIPULACION DE DATOS
@author: jmao
"""
import pandas as pd

path = "/home/jmao/Mao_files/DataKnow_Prueba_Tecnica/Datos3/"
path_save = "/home/jmao/Mao_files/DataKnow_Prueba_Tecnica/Resultados/Anexos_Prueba_2/"

headers = ["Central","Hora_1","Hora_2","Hora_3", "Hora_4","Hora_5",
         "Hora_6","Hora_7","Hora_8", "Hora_9","Hora_10","Hora_11","Hora_12","Hora_13",
         "Hora_14", "Hora_15","Hora_16","Hora_17","Hora_18","Hora_19","Hora_20",
         "Hora_21","Hora_22","Hora_23","Hora_24"]

df = pd.read_excel(path + "Datos Maestros VF.xlsx" , sheet_name="Master Data Oficial") #Open xlsx file and select sheet_name


#Filter df.DataFrame to display specific column data. 
df= df[["Nombre visible Agente","AGENTE (OFEI)", "CENTRAL (dDEC, dSEGDES, dPRU…)","Tipo de central (Hidro, Termo, Filo, Menor)" ]]

#Filter informacion to display only data with visible agent "EMGESA
df = df[df["Nombre visible Agente"] == "EMGESA"]

#Create two DataFrame with specifil Central type "H" and "T"
df_H = df[df["Tipo de central (Hidro, Termo, Filo, Menor)"] == "H"]
df_T = df[df["Tipo de central (Hidro, Termo, Filo, Menor)"] == "T"]

#Store in a list the two previous dataframes
to_conca = [df_H, df_T]

#Concatenate the two dataframes and reset indices
df = pd.concat(to_conca)
df = df.reset_index(drop=True) # Reset data indices
#-----------------------------------------------------------------------------
#Piece of code to open .txt file and convert it to DataFrame

split_by_clm = []

with open(path+ "dDEC1204.TXT",'r') as file:   #Open and read.txt file
   data = file.read() 
   
split_by_row = data.split("\n") #row separation by \n variable

for index in split_by_row:
    split_by_clm.append(index.split(',')) #Column separation by comma variable

table = pd.DataFrame(split_by_clm) #List to DataFrame convertion

#------------------------------------------------------------------------------
#For loop to compare two central columns in Datos Maestros. xlsx and dDEC1204.txt with the purpose to add 
# the information of hour 1 -  hour 24
dummy_table = [None] * len(df)
for i in range(len(df)):
    for j in range(len(table)):
        if df["CENTRAL (dDEC, dSEGDES, dPRU…)"][i] == table[0][j]:
            dummy_table[i] = table[table[0] == df["CENTRAL (dDEC, dSEGDES, dPRU…)"][i]]
# The information is concatenated
table = pd.concat(dummy_table)
table.columns = headers
table = table.reset_index(drop=True) # Reset data indices (some cases is needed, this is one of them)

#Merge tables (xlsx and txt filtered information)
df_table_merge = pd.concat([df, table], axis=1)

#Drop extra "Central" column added from dummy_table 
df_table_merge.drop("Central", axis=1, inplace=True)

#Perform Horizontal Sum. But before is important to check and change the datatype. 
#With the next for-loop the data type of hour columns is change to int to be able to compute the operation
for data in range(1,len(headers)):
    df_table_merge[headers[data]] = df_table_merge[headers[data]].astype("int")

#perform sum of each row
hor_sum = df_table_merge.sum(axis=1)
#Create new column with result of the sum
df_table_merge["Total_HORA"] = hor_sum
#Select/filter register different than zero
df_table_merge = df_table_merge[df_table_merge["Total_HORA"] != 0]
df_table_merge.to_excel(path_save + "Output_Man_datos.xlsx") #Data saved in xlsx format