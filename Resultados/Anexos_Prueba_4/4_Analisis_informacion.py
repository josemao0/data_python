#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 23 15:08:33 2019

@author: jmao
"""
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
path = "/home/jmao/Mao_files/DataKnow_Prueba_Tecnica/Datos3/"
path_save = "/home/jmao/Mao_files/DataKnow_Prueba_Tecnica/Resultados/Anexos_Prueba_4/"

#Se importa los archivos .csv para tratamiento de los datos.
df_barrios = pd.read_csv(path + "exa__barrios_cali.csv")
df_dispositivos = pd.read_csv(path + "exa__dispositivos_cali.csv")
df_cliente = pd.read_csv(path + "exa__trx_clientes.csv")

#ESTA PRIMERA PARTE DE CODIGO SE EJECUTA LA PRIMERA VEZ PARA AGREGAR LOS DATOS DE LOS BARRIOS EN EL ARCHIVO
#  "exa__dispositivos_cali.csv" Y "exa__trx_clientes.csv".
# EL RESULTADO DE ESTA PARTE SE ENCUENTRA EN LA CARPETA DE RESULTADOS CON LOS SIGUIENTES NOMBRES: 
# - "Output_clientes_datos.xlsx"
# - "Output_dispositivos.xlsx"

# Esta parte del código selecciona columnas específicas de cada dataframe y las convierte en listas
# para una más rápida ejecución del proceso. Se usa for-loop para comparar código de barrios y código de 
# dispositivos. Las listas obtenidas del for-loop se conviernten en dataframe y se le asigna un nombre a la columna
# para luego concatenar la columna de los barrios a los archivos de los dispositivos y clientes. 
##################################################################################################################
#dummy_barrios_disp = [None] * len(df_dispositivos)
#
#lista_barrios = list(df_barrios["nombre"])
#lista_codigos_barrio = list(df_barrios["codigo"])
#lista_barrio_disp = list(df_dispositivos["id_barrio"])
#
#for i in range(len(df_dispositivos)):
#    for j in range(len(df_barrios)):
#        if  lista_codigos_barrio[j] ==  lista_barrio_disp[i]:
#            dummy_barrios_disp[i] = lista_barrios[j]
#
#dummy_barrios_disp = pd.DataFrame(dummy_barrios_disp,columns=["barrios_disp"])
#
##Merge tables
#df_dispositivos = pd.concat([df_dispositivos, dummy_barrios_disp], axis=1)
##filter zones-neighborhoods found in client csv 
#df_dispositivos = df_dispositivos.reset_index(drop=True) # Reset data indices
#df_dispositivos.to_excel(path_save+"Output_dispositivos.xlsx", sheet_name="Sheet1")
##
#dummy_table_cliente = [None] * len(df_cliente)
#lista_barrios_disp_cliente = list(df_dispositivos["barrios_disp"])
#lista_codigos_disp = list(df_dispositivos["codigo"])
#lista_codigos_cliente = list(df_cliente["cod_dispositivo"])
#
#for i in range(len(df_cliente)):
#    for j in range(len(df_dispositivos)):
#        if lista_codigos_disp[j] == lista_codigos_cliente[i]:
#            dummy_table_cliente[i] = lista_barrios_disp_cliente[j]
#            
#dummy_table_cliente =  pd.DataFrame(dummy_table_cliente, columns=["barrios_cliente"])
#df_cliente = pd.concat([df_cliente, dummy_table_cliente], axis=1)
#columna_porcentaje = df_cliente["num_trx"]*100/sum(df_cliente["num_trx"])
#df_cliente["Porcentaje_transacciones"] = columna_porcentaje
#df_cliente.to_excel(path_save + "Output_clientes_datos.xlsx") #Data saved in xlsx format
################################################################################################

# Los archivos generados del proceso anterior se usan para el procesamiento y analisis de los datos. 
df_cliente = pd.read_excel(path_save+"Output_clientes_datos.xlsx", sheet_name="Sheet1")
df_dispositivos = pd.read_excel(path_save+"Output_dispositivos.xlsx", sheet_name="Sheet1")

#El uso de pivot tables es útil para relacionar datos que se repiten en casos específicos
#Las tres siguientes Pivot tables se generar para analizar la concurrencia de los dispositivos
# y la concurrencia de las transacciones hechas por los clientes en cada zona/barrio de la ciudad de Cali
tabla_zonas_cliente =  pd.pivot_table(df_cliente, values = ["num_trx","Porcentaje_transacciones"], index= ["num_doc","barrios_cliente"], aggfunc=np.sum, fill_value=0)
tabla_zonas_concurridas = pd.pivot_table(df_cliente, values = "num_trx", index= "barrios_cliente", aggfunc=np.sum, fill_value=0)
tabla_dispositivos = pd.pivot_table(df_dispositivos, index= "barrios_disp", columns= "tipo", aggfunc="size")

#En caso que se requiera se guardan la pivot table que relaciona las zonas concurridas y los dispositivos por barrio. 
tabla_zonas_concurridas.to_csv(path_save + "tabla_zonas_concurridas.csv") #Data saved in xlsx format
tabla_dispositivos.to_csv(path_save + "tabla_barrios_dispositivos.csv") #Data saved in xlsx format

#La pivot table de zona de clientes se guarda en un archivo .csv 
tabla_zonas_cliente.to_csv(path_save + "zonas_de_influencia.csv") #Data saved in xlsx format


#Plot data de zonas concurridas - Quitar comentario para ver figura de barra horizontal
#tabla_zonas_concurridas[tabla_zonas_concurridas["num_trx"] > 2000].plot(kind = "barh",title ="Zonas más concurridas de la Ciudad de Cali", fontsize = 6)
#plt.savefig(path_save+'Zonas_Concurridas.png', dpi = 800)

##############################################################################################################3
# Por medio de la pivot table "tabla_dispositivos" se filtra cada dispositivo para un analisis individual
# Se filtra la información que no arroje ningun valor "NaN"/"0" por medio del comando "dropna" 
# Se realiza la gráfica de cada dispositivo, en algunos es necesario condicionar para mostrar solo los valores más
# representativos. 

#Quitar comentario de las dos últimas lineas de cada tabla de dispositivo para ver la grafica de barras horizontal

CB_TABLA = pd.DataFrame(tabla_dispositivos["CB"])
CB_TABLA.dropna(subset=["CB"], axis=0, inplace=True)
#CB_TABLA[CB_TABLA["CB"] > 50].plot(kind = "barh",title ="Dispositivos CB en la ciudad de Cali", fontsize = 5)
#plt.savefig(path_save+'Dispositivos_CB.png', dpi = 800)

DISPENSADOR_TABLA = pd.DataFrame(tabla_dispositivos["DISPENSADOR"])
DISPENSADOR_TABLA.dropna(subset=["DISPENSADOR"], axis=0, inplace=True)
#DISPENSADOR_TABLA[DISPENSADOR_TABLA["DISPENSADOR"] > 250].plot(kind = "barh",title ="Dispositivos DISPENSADOR en la ciudad de Cali", fontsize = 5)
#plt.savefig(path_save+'Dispositivos_DISPENSADOR.png', dpi = 800)

MF_TABLA = pd.DataFrame(tabla_dispositivos["MF"])
MF_TABLA.dropna(subset=["MF"], axis=0, inplace=True)
#MF_TABLA.plot(kind = "barh",title ="Dispositivos MF en la ciudad de Cali", fontsize = 8)
#plt.savefig(path_save+'Dispositivos_MF.png', dpi = 800)

PAC_TABLA = pd.DataFrame(tabla_dispositivos["PAC"])
PAC_TABLA.dropna(subset=["PAC"], axis=0, inplace=True)
#PAC_TABLA.plot(kind = "barh",title ="Dispositivos PAC en la ciudad de Cali", fontsize = 8)
#plt.savefig(path_save+'Dispositivos_PAC.png', dpi = 800)

POS_TABLA = pd.DataFrame(tabla_dispositivos["POS"])
POS_TABLA.dropna(subset=["POS"], axis=0, inplace=True)
#POS_TABLA[POS_TABLA["POS"] > 250].plot(kind = "barh",title ="Dispositivos POS en la ciudad de Cali", fontsize = 8)
#plt.savefig(path_save+'Dispositivos_POS.png', dpi = 800)

SAI_TABLA = pd.DataFrame(tabla_dispositivos["SAI"])
SAI_TABLA.dropna(subset=["SAI"], axis=0, inplace=True)
#SAI_TABLA.plot(kind = "barh",title ="Dispositivos SAI en la ciudad de Cali", fontsize = 8)
#plt.savefig(path_save+'Dispositivos_SAI.png', dpi = 800)
####################################################################################################################
#IDENTIFICACION DE LAS ZONAS DISTINTAS A LOS BARRIOS

#importa tabla dispositivos en los barrios
Disp_in_barrios = pd.read_csv(path_save + "tabla_barrios_dispositivos.csv")

#Por medio del comando "isin" (negado) se filtra la informacion de las zonas distintas a los barrios 
# relacionados con los dispositivos/clientes. 
# El archivo resultante se guarda en el archivo "Zonas_distintas.csv"
Zonas_distintas = df_barrios[~df_barrios["nombre"].isin(Disp_in_barrios["barrios_disp"])]
Zonas_distintas = Zonas_distintas.reset_index(drop=True) # Reset data indices
Zonas_distintas.to_csv(path_save + "Zonas_distintas.csv") #Data saved in csv format